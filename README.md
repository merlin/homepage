# Tardis Homepage
Static Homepage for [Tardis Project](https://tardisproject.uk).

## Developing

We use [Astro](https://vitejs.dev/) for building the site. Assuming you have node installed, you can probably just:

```
$ yarn install --dev
$ yarn dev
```
