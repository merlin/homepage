---
title: Expose apps even easier
pubDate: 2023-06-01
---

Before, if you wanted to expose an app on Tardis to the internet, it had to be a static app or you had to go through Kubernetes (even if it wasn't deployed on there).

Now, you can set it up anywhere in our internal network, and easily make a reverse proxy entry for it from the [tardis console](https://console.tardisproject.uk).

For example, if you're using port 9342 on the sandbox vm, you can make an entry from `/my-app` to address `192.168.0.8` and port `9342`. For now, you'll be able to access your app at `https://<username>.tardis.ac/my-app`, with HTTPs and everything handled automatically.
